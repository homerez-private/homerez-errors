'use strict';

var _ = require('lodash');

// Actually used:
// DataIntegrityError
// EntityNotFoundError
// ExternalApiError
// FieldValidationError
// InvalidNameError
// SystemError
// AccessDeniedError

var knownErrors = [
  'ERROR_INVALID_NAME',
  'ERROR_INVALID_ARGUMENTS',
  'ERROR_ENTITY_NOT_FOUND',
  'ERROR_FIELD_VALIDATION',
  'ERROR_EXTERNAL_API',
  'ERROR_DATA_INTEGRITY',
  'ERROR_SYSTEM',
  'ERROR_ACCESS_DENIED',
  'ERROR_UNKNOWN'
];

var nameToErrorCode = {
  'ERROR_ENTITY_NOT_FOUND': 100,
  'ERROR_DATA_INTEGRITY'  : 120,
  'ERROR_INVALID_NAME'    : 200,
  'ERROR_INVALID_ARGUMENTS': 201,
  'ERROR_FIELD_VALIDATION': 202,
  'ERROR_EXTERNAL_API'    : 601,
  'ERROR_SYSTEM'          : 600,
  'ERROR_ACCESS_DENIED'   : 201,
  'ERROR_UNKNOWN'         : 999
};

/**
 * A DataIntegrityError indicates that the given arguments lead to a problem with the
 * integrity of some data. For example, having a start date later in time than an end
 * date for a date period.
 * 
 * @param {String} field   The field that has an integrity error.
 * @param {String} message The message describing what could not be found.
 */

function DataIntegrityError(field, message) {
  this.type    = 'ERROR_DATA_INTEGRITY';
  this.field   = field;
  this.message = message;
  this.jsonrpc = {
    code: nameToErrorCode[this.type],
    message: 'Data integrity error in ' + field + ': ' + message,
    data: {
      field: field
    }
  }
}

DataIntegrityError.prototype = new Error();
DataIntegrityError.prototype.constructor = DataIntegrityError;


/**
 * An EntityNotFoundError indicates that the calling code was looking for a specific
 * entity (e.g. a property, inquiry or rate), mostly by its id, which does not exist
 * in the database.
 * 
 * @param {String} entity  The entity that could not be found.
 * @param {String} message The message describing what could not be found.
 */

function EntityNotFoundError(entity, message) {
  this.type    = 'ERROR_ENTITY_NOT_FOUND';
  this.entity  = entity;
  this.message = message;
  this.jsonrpc = {
    code: nameToErrorCode[this.type],
    message: entity + ' not found: ' + message,
    data: {
      entity: entity
    }
  };
}

EntityNotFoundError.prototype = new Error();
EntityNotFoundError.prototype.constructor = EntityNotFoundError;

/**
 * An InvalidNameError indicates a given name was not found or incorrect, for example
 * mentioning a property's connector by an non-existing name.
 * 
 * @param {String} field   The field in which the error occurred.
 * @param {String} message The message describing what could not be found.
 */

function InvalidNameError(field, message) {
  this.type    = 'ERROR_INVALID_NAME';
  this.field   = field;
  this.message = message;
  this.jsonrpc = {
    code: nameToErrorCode[this.type],
    message: 'Invalid name in field ' + field + ': ' + message,
    data: {
      field: field
    }
  };
}

InvalidNameError.prototype = new Error();
InvalidNameError.prototype.constructor = InvalidNameError;

/**
 * An InvalidArgumentsError is used when the arguments of a function are
 * invalid. This is closely coupled with the validate.js module.
 * 
 * @param {String} message The message describing the error(s).
 * @param {String} data The validate.js error object.
 */

function InvalidArgumentsError(message, data) {
  this.type    = 'ERROR_INVALID_ARGUMENTS';
  this.message = message;
  this.data    = data;
  this.jsonrpc = {
    code: nameToErrorCode[this.type],
    message: 'Invalid argument(s): ' + message,
    data: data
  };
}

InvalidArgumentsError.prototype = new Error();
InvalidArgumentsError.prototype.constructor = InvalidArgumentsError;

// TODO: merge FieldValidationError and InvalidValueError
// function InvalidValueError(field, message) {
//   this.type    = 'ERROR_INVALID_VALUE';
//   this.field   = field;
//   this.message = message;
//   this.jsonrpc = {
//     code: nameToErrorCode[this.type],
//     message: 'Invalid value in field ' + field + ': ' + message,
//     data: {
//       field: field
//     }
//   };
// }

// InvalidValueError.prototype = new Error();
// InvalidValueError.prototype.constructor = InvalidValueError;

// TODO: merge FieldValidationError and InvalidValueError
function FieldValidationError(field, message) {
  this.type    = 'ERROR_FIELD_VALIDATION';
  this.field   = field;
  this.message = message;
  this.jsonrpc = {
    code: nameToErrorCode[this.type],
    message: 'Invalid value in field ' + field + ': ' + message,
    data: {
      field: field
    }
  };
}

FieldValidationError.prototype = new Error();
FieldValidationError.prototype.constructor = FieldValidationError;

function ExternalApiError(name, message) {
  this.type    = 'ERROR_EXTERNAL_API';
  this.name    = name;
  this.message = message;
  this.jsonrpc = {
    code: nameToErrorCode[this.type],
    message: 'External API error for ' + name + ': ' + message,
    data: {
      name: name
    }
  };
}

ExternalApiError.prototype = new Error();
ExternalApiError.prototype.constructor = ExternalApiError;

function SystemError(name, message) {
  this.type    = 'ERROR_SYSTEM';
  this.name    = name;
  this.message = message;
  this.jsonrpc = {
    code: nameToErrorCode[this.type],
    message: 'System error for ' + name + ': ' + message,
    data: {
      name: name
    }
  };
}

SystemError.prototype = new Error();
SystemError.prototype.constructor = SystemError;

function AccessDeniedError(username, message) {
  this.type    = 'ERROR_ACCESS_DENIED';
  this.name    = username;
  this.message = message;
  this.jsonrpc = {
    code: nameToErrorCode[this.type],
    message: 'Access Denied for ' + username + ': ' + message,
    data: {
      name: username
    }
  };
}

AccessDeniedError.prototype = new Error();
AccessDeniedError.prototype.constructor = AccessDeniedError;

function UnknownError(message) {
  this.type    = 'ERROR_UNKNOWN';
  this.message = message;
  this.jsonrpc = {
    code: nameToErrorCode[this.type],
    message: 'Unknown error: ' + message,
    data: {
      message: message
    }
  };
}

UnknownError.prototype = new Error();
UnknownError.prototype.constructor = UnknownError;

// For now, there are 2 API errors:
// ERROR_INVALID_INPUT: the api call got some incorrect input
// ERROR_ENTITY_NOT_FOUND: the api call could not find the entity (property, owner)

function convertToApiError(e) {
  var apiError;
  if (e && e.type && _.some(knownErrors, function(error) { return error === e.type; })) {
    apiError = {
      status: 200,
      type: e.type,
      message: e.message
    };
    if (apiError.type === 'ERROR_INVALID_NAME') {
      apiError.type = 'ERROR_INVALID_INPUT';
    }
    if (apiError.type === 'ERROR_FIELD_VALIDATION') {
      apiError.type = 'ERROR_INVALID_INPUT';
    }
  }
  else {
    apiError = {
      status: 500,
      type: 'ERROR_UNKNOWN',
      message: e.message
    };
  }

  return apiError;
}

// Check if this is being run under node. If so, only then
// we can assign to module.exports.
if (typeof(window) === 'undefined') {
  module.exports = {
    convertToApiError   : convertToApiError,

    DataIntegrityError  : DataIntegrityError,
    EntityNotFoundError : EntityNotFoundError,
    ExternalApiError    : ExternalApiError,
    FieldValidationError: FieldValidationError,
    InvalidArgumentsError: InvalidArgumentsError,
    InvalidNameError    : InvalidNameError,
    SystemError         : SystemError,
    AccessDeniedError   : AccessDeniedError,
    UnknownError        : UnknownError
  };
}
