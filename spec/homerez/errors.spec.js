'use strict';

var should = require('should');
var HE = require('../../src/homerez/errors');

describe('HomeRez Errors', function() {
  it('should create a DataIntegrityError', function(done) {
    // given
    var field = 'end_date';
    var message = 'end_date (2015-05-30) needs to be after begin_date (2015-06-01)';

    // when
    var e = new HE.DataIntegrityError(field, message);

    // then
    e.field.should.equal(field);
    e.type.should.equal('ERROR_DATA_INTEGRITY');
    e.message.should.equal(message);
    e.jsonrpc.code.should.equal(120);
    e.jsonrpc.message.should.equal('Data integrity error in ' + field + ': ' + message);
    done();
  });

  it('should create a EntityNotFoundError', function(done) {
    // given
    var entity = 'property';
    var message = 'property with id=12345678 does not exist';

    // when
    var e = new HE.EntityNotFoundError(entity, message);

    // then
    e.entity.should.equal(entity);
    e.type.should.equal('ERROR_ENTITY_NOT_FOUND');
    e.message.should.equal(message);
    e.jsonrpc.code.should.equal(100);
    e.jsonrpc.message.should.equal('property not found: ' + message);
    done();
  });

  it('should create a InvalidNameError', function(done) {
    // given
    var field = 'connectorName';
    var message = 'The connector with name=kogi does not exist.';

    // when
    var e = new HE.InvalidNameError(field, message);

    // then
    e.field.should.equal(field);
    e.type.should.equal('ERROR_INVALID_NAME');
    e.message.should.equal(message);
    e.jsonrpc.code.should.equal(200);
    e.jsonrpc.message.should.equal('Invalid name in field ' + field + ': ' + message);
    done();
  });

  it('should create an InvalidArgumentsError', function(done) {
    // given
    var message = 'propertyId cannot be blank.';
    var data    = { propertyId: ['propertyId cannot be blank'] };

    // when
    var e = new HE.InvalidArgumentsError(message, data);

    // then
    e.data.propertyId[0].should.equal(data.propertyId[0]);
    e.type.should.equal('ERROR_INVALID_ARGUMENTS');
    e.message.should.equal(message);
    e.jsonrpc.code.should.equal(201);
    e.jsonrpc.message.should.equal('Invalid argument(s): ' + message);
    done();
  });

  it('should create a FieldValidationError', function(done) {
    // given
    var field = 'phoneNumber';
    var message = 'This is not a valid phone number: "banana"';

    // when
    var e = new HE.FieldValidationError(field, message);

    // then
    e.field.should.equal(field);
    e.type.should.equal('ERROR_FIELD_VALIDATION');
    e.message.should.equal(message);
    e.jsonrpc.code.should.equal(202);
    e.jsonrpc.message.should.equal('Invalid value in field ' + field + ': ' + message);
    done();
  });

  it('should create a ExternalApiError', function(done) {
    // given
    var name = 'phoneNumber';
    var message = 'This is not a valid phone number: "banana"';

    // when
    var e = new HE.ExternalApiError(name, message);

    // then
    e.name.should.equal(name);
    e.type.should.equal('ERROR_EXTERNAL_API');
    e.message.should.equal(message);
    e.jsonrpc.code.should.equal(601);
    e.jsonrpc.message.should.equal('External API error for ' + name + ': ' + message);
    done();
  });

  it('should create a SystemError', function(done) {
    // given
    var name = 'phoneNumber';
    var message = 'This is not a valid phone number: "banana"';

    // when
    var e = new HE.SystemError(name, message);

    // then
    e.name.should.equal(name);
    e.type.should.equal('ERROR_SYSTEM');
    e.message.should.equal(message);
    e.jsonrpc.code.should.equal(600);
    e.jsonrpc.message.should.equal('System error for ' + name + ': ' + message);
    done();
  });

  it('should create a AccessDeniedError', function(done) {
    // given
    var username = 'johndoe';
    var message = 'can\'t access property 12345678'

    // when
    var e = new HE.AccessDeniedError(username, message);

    // then
    e.name.should.equal(username);
    e.type.should.equal('ERROR_ACCESS_DENIED');
    e.message.should.equal(message);
    e.jsonrpc.code.should.equal(201);
    e.jsonrpc.message.should.equal('Access Denied for ' + username + ': ' + message);
    done();
  });

  it('should create a UnknownError', function(done) {
    // given
    var message = 'Something completely weird happened';

    // when
    var e = new HE.UnknownError(message);

    // then
    e.type.should.equal('ERROR_UNKNOWN');
    e.message.should.equal(message);
    e.jsonrpc.code.should.equal(999);
    e.jsonrpc.message.should.equal('Unknown error: ' + message);
    done();
  });



});
